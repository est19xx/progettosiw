package it.progetto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import it.progetto.model.Responsabile;

public interface ResponsabileRepository extends JpaRepository<Responsabile, Long>{
     Responsabile findByName(String nome);

}
