package it.progetto.repository;

import org.springframework.data.repository.CrudRepository;

import it.progetto.model.Azienda;

public interface AziendaRepository extends CrudRepository<Azienda, Long> {

}
