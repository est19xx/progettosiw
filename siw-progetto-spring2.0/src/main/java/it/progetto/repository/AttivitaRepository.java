package it.progetto.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.progetto.model.Attivita;
import it.progetto.model.Centro;

public interface AttivitaRepository extends CrudRepository<Attivita, Long> {

	public List<Attivita> findByDate(Date date);
	
	public List<Attivita> findByCentroAndDate(Centro centro, Date date);
	
	public List<Attivita> findByCentro(Centro centro);

}
