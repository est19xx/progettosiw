package it.progetto.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.progetto.model.Allievo;

public interface AllievoRepository extends CrudRepository<Allievo, Long> {
	
	//Abbiamo già a disposizione dei metodi come : save, update delate....(vedere documentazione)
	//Non dobbiamo scrivere l'imlentazione con l'entity manager....
	
	//Non dobbiamo scrivere la query implementando questa interfaccia, ma seguendo la convezione sul nome del metodo
	//scrivendo findByNOMEATTRIBUTO(String NOMEATTRIBUTO) (l'attributo deve stare nell'entintà) 
	//Possiamo in pratica comporre la query tramite la convezione 
	public List<Allievo> findByCity(String city);

	public List<Allievo> findByNameAndSurnameAndCity(String name, String surname, String city);
	
	public Allievo findByName(String name);

}
