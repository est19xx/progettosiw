package it.progetto.repository;

import org.springframework.data.repository.CrudRepository;

import it.progetto.model.Centro;

public interface CentroRepository extends CrudRepository<Centro, Long> {
	

}
