package it.progetto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.progetto.model.Centro;
import it.progetto.repository.CentroRepository;

@Transactional
@Service
public class CentroService {
	
	@Autowired
	private CentroRepository centroRepository;

	public Centro save(Centro centro) {
		return this.centroRepository.save(centro);
	}

	public List<Centro> findAll() {
		return (List<Centro>) this.centroRepository.findAll();
	}


	


}
