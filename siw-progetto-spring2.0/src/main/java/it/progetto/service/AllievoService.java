package it.progetto.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.progetto.model.Allievo;
import it.progetto.model.Attivita;
import it.progetto.repository.AllievoRepository;

@Transactional
@Service
public class AllievoService {
	
	@Autowired//Ci dice che lo istanzia il contenitore(AllievoDaoImpl è un interfaccia di cui lui si trova l'istanza)
	private AllievoRepository allievoRepository; 
	
	public Allievo save(Allievo allievo) {
		return this.allievoRepository.save(allievo);
	}

	public List<Allievo> findByCity(String city) {
		return this.allievoRepository.findByCity(city);
	}

	public List<Allievo> findAll() {
		return (List<Allievo>) this.allievoRepository.findAll();
	}
	
	public Allievo findById(Long id) {
		Optional<Allievo> allievo = this.allievoRepository.findById(id);
		if (allievo.isPresent()) 
			return allievo.get();
		else
			return null;
	}

	public boolean alreadyExists(Allievo allievo) {
		List<Allievo> allievi = this.allievoRepository.findByNameAndSurnameAndCity(allievo.getName(), allievo.getSurname(), allievo.getCity());
		if (allievi.size() > 0)
			return true;
		else 
			return false;
	}	
	
	public Allievo findByName(String name) {
		return this.allievoRepository.findByName(name);
	}

	public void update(Allievo allievoCorrente, Attivita attivitaCorrente) {
		if(allievoCorrente.getAttivita()==null) {
			List<Attivita> attivitaAllievo = new ArrayList<>();
			attivitaAllievo.add(attivitaCorrente);
			allievoCorrente.setAttivita(attivitaAllievo);
			this.allievoRepository.save(allievoCorrente);
		}
		else {
			allievoCorrente.getAttivita().add(attivitaCorrente);
			this.allievoRepository.save(allievoCorrente);
		}
		
		
	}

}
