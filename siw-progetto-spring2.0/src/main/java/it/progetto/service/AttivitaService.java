package it.progetto.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.progetto.model.Allievo;
import it.progetto.model.Attivita;
import it.progetto.model.Centro;
import it.progetto.repository.AttivitaRepository;

@Transactional
@Service
public class AttivitaService {

	@Autowired
	private AttivitaRepository attivitaRepository;

	public Attivita save(Attivita attivita) {
		return this.attivitaRepository.save(attivita);
	}

	public List<Attivita> findAll() {
		return (List<Attivita>) this.attivitaRepository.findAll();
	}

	public List<Attivita> findByDate(Date date) {
		return this.attivitaRepository.findByDate(date);
	}
	
	public Attivita findById(Long id) {
		Optional<Attivita> attivita = this.attivitaRepository.findById(id);
		if (attivita.isPresent()) 
			return attivita.get();
		else
			return null;
	}

	public List<Attivita> findByCentroAndDate(Centro centroCorrente, Date date) {
		List<Attivita> attivita= this.attivitaRepository.findByCentroAndDate(centroCorrente, date);
		return attivita;
	}
	
	public List<Attivita> findByCentro(Centro centro) {
		return this.attivitaRepository.findByCentro(centro);
	}

	public void update(Attivita attivitaCorrente, Allievo allievoCorrente) {
		if(attivitaCorrente.getAllievi()==null) {
			List<Allievo> allieviAttivita = new ArrayList<>();
			allieviAttivita.add(allievoCorrente);
			attivitaCorrente.setAllievi(allieviAttivita);
			this.attivitaRepository.save(attivitaCorrente);
		}
		else {
			attivitaCorrente.getAllievi().add(allievoCorrente);
			this.attivitaRepository.save(attivitaCorrente);
		}
		
	}
	
	
	
	

}
