package it.progetto.service;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.progetto.model.Attivita;
import it.progetto.model.Centro;
import it.progetto.model.Responsabile;
import it.progetto.model.Role;
import it.progetto.repository.AttivitaRepository;
import it.progetto.repository.ResponsabileRepository;
import it.progetto.repository.RoleRepository;

@Transactional
@Service
public class ResponsabileService {
	
	@Autowired
	private ResponsabileRepository responsabileRepository;
	
	@Autowired
    private RoleRepository roleRepository;
	
	@Autowired
    private AttivitaRepository attivitaRepository;
	
	 @Autowired
	 private BCryptPasswordEncoder bCryptPasswordEncoder;
	 
	 @Autowired
	 private CentroService centroService;

	public Responsabile save(Responsabile responsabile) {
		return this.responsabileRepository.save(responsabile);
	}
	
	public Responsabile findByName(String nome) {
		return this.responsabileRepository.findByName(nome);
	}
	
	
	public void saveResponsabile(Responsabile responsabile) {
		responsabile.setPassword(bCryptPasswordEncoder.encode(responsabile.getPassword()));
        responsabile.setActive(1);
        Role userRole = roleRepository.findByRole("USER");
        Centro centro = new Centro("Roma3", "Via della vasca navale", "uniroma3@gmail.com", "0657332100", 150);
        
        Attivita attivita1 = new Attivita("Progetto", new Date(96, 5, 4), new Date() );
        attivita1.setCentro(centro);
        
        attivitaRepository.save(attivita1);
        
        centroService.save(centro);
        responsabile.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        responsabile.setCentro(centro);
		responsabileRepository.save(responsabile);
	}



}
