package it.progetto;


import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import it.progetto.model.Role;
import it.progetto.repository.RoleRepository;

@SpringBootApplication
public class SiwDemoSpringApplication {

	@Autowired
	private RoleRepository roleRepository;

	public static void main(String[] args) {
		SpringApplication.run(SiwDemoSpringApplication.class, args);
	}

	@PostConstruct
	public void init() {

		Role role = new Role(1, "USER");
		roleRepository.save(role);

	}

}
