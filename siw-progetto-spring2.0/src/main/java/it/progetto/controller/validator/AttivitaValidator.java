package it.progetto.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.progetto.model.Attivita;

@Component
public class AttivitaValidator implements Validator {

	   @Override
	    public void validate(Object o, Errors errors) {
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "date", "required");
	      
	    }

	    @Override
	    public boolean supports(Class<?> aClass) {
	        return Attivita.class.equals(aClass);
	    }	



}
