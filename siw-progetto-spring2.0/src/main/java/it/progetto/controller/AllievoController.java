package it.progetto.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.progetto.controller.validator.AllievoValidator;
import it.progetto.model.Allievo;
import it.progetto.service.AllievoService;

@Controller
public class AllievoController {

	@Autowired
	private AllievoService allievoService;

	@Autowired
	private AllievoValidator validator;

	@RequestMapping("/allievi")
	public String allievi(Model model) {
		model.addAttribute("allievi", this.allievoService.findAll());
		model.addAttribute("service", this.allievoService);
		return "allievoList";
	}

	@RequestMapping("/addAllievo")
	public String addAllievo(Model model) {
		model.addAttribute("allievo", new Allievo());
		return "allievoForm";
	}

	@RequestMapping(value = "/allievo/{id}", method = RequestMethod.GET)
	public String getAllievo(@PathVariable Long id, Model model) {
		model.addAttribute("allievo", this.allievoService.findById(id));
		return "showAllievo";
	}

	@RequestMapping(value = "/allievo", method = RequestMethod.POST)
	public String newAllievo(@Valid @ModelAttribute Allievo allievo, Model model, BindingResult bindingResult) {

		this.validator.validate(allievo, bindingResult);

		if (this.allievoService.alreadyExists(allievo)) {
			model.addAttribute("exists", "Allievo already exists");
			return "allievoForm";
		} else {
			if (!bindingResult.hasErrors()) {
				this.allievoService.save(allievo);
				model.addAttribute("allievo", this.allievoService.findById(allievo.getId()));
				return "allievoInserito";
			}
		}
		return "allievoForm";
	}

	@RequestMapping("/ricercaAllievo")
	public String scegliAllievo(Model model) {
		model.addAttribute("allievo", new Allievo());
		return "ricercaAllievo";
	}

	@RequestMapping(value = "/cercaAllievo", method = RequestMethod.POST)
	public String cercaAllievo(@ModelAttribute Allievo allievo, Model model) {

		Allievo allievoCercato = this.allievoService.findById(allievo.getId());

		if (allievoCercato != null) {
			model.addAttribute("allievo", allievoCercato);

			return "showAllievo";
		}

		else {
			model.addAttribute("notExists", "Allievo non trovato");
			return "ricercaAllievo";
		}

	}

}
