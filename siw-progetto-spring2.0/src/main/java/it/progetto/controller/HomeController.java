package it.progetto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import it.progetto.model.Responsabile;
import it.progetto.service.ResponsabileService;

@Controller
public class HomeController {
	
	@Autowired
	private ResponsabileService userService;
	


	@RequestMapping("/home")
	public String home(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Responsabile user = userService.findByName(auth.getName());
		model.addAttribute("responsabileCorrente", user);
		model.addAttribute("centroCorrente", user.getCentro());
		return "home";
	}
	
	
	

}
