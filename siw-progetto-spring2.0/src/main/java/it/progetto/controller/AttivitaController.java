package it.progetto.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import it.progetto.controller.validator.AttivitaValidator;
import it.progetto.model.Allievo;
import it.progetto.model.Attivita;
import it.progetto.model.Centro;
import it.progetto.model.Responsabile;
import it.progetto.service.AllievoService;
import it.progetto.service.AttivitaService;
import it.progetto.service.CentroService;
import it.progetto.service.ResponsabileService;

@Controller
@SessionAttributes("allievo")
public class AttivitaController {

	@Autowired
	private AttivitaService attivitaService;

	@Autowired
	private AllievoService allievoService;

	@Autowired
	private AttivitaValidator validator;

	@Autowired
	private ResponsabileService userService;

	@Autowired
	private CentroService centroService;

	@RequestMapping("/attivita")
	public String attivita(Model model) {
		model.addAttribute("attivita", this.attivitaService.findAll());
		return "attivitaList";
	}

	@RequestMapping("/addAttivita")
	public String addAllievo(Model model) {
		model.addAttribute("attivita", new Attivita());
		return "attivitaForm";
	}

	@RequestMapping(value = "/nuovaAttivita", method = RequestMethod.POST)
	public String newAttivita(@Valid @ModelAttribute Attivita attivita, Model model, BindingResult bindingResult) {

		this.validator.validate(attivita, bindingResult);

		if (!bindingResult.hasErrors()) {

			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			Responsabile user = userService.findByName(auth.getName());

			Centro centroCorrente = user.getCentro();
			List<Attivita> attivitaCentro = centroCorrente.getAttivita();
			attivitaCentro.add(attivita);

			attivita.setCentro(centroCorrente);

			this.attivitaService.save(attivita);
			this.centroService.save(centroCorrente);

			model.addAttribute("attivita", attivita);
			return "attivitaInserita";
		} else {
			return "attivitaForm";
		}

	}

	@RequestMapping("/ricercaAttivita")
	public String scegliAttivita(Model model) {
		model.addAttribute("attivita", new Attivita());
		return "ricercaAttivita";
	}

	@RequestMapping(value = "/cercaAttivita", method = RequestMethod.POST)
	public String cercaAllievo(@ModelAttribute Attivita attivita, Model model) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Responsabile user = userService.findByName(auth.getName());

		Centro centroCorrente = user.getCentro();

		List<Attivita> attivitaCercate = this.attivitaService.findByCentroAndDate(centroCorrente,
				attivita.getDate());

		if (attivitaCercate.size()>0) {
			model.addAttribute("attivita", attivitaCercate);
			return "attivitaList";
		}

		else {
			model.addAttribute("notExists", "Non sono state svolte attivita in questa data");
			return "ricercaAttivita";
		}

	}

	@RequestMapping("/prenotazioneAttivita")
	public String scegliAllievo(Model model) {
		model.addAttribute("allievo", new Allievo());// forse è da levare
		return "prenotazione";
	}

	// Sarebbe meglio usare un metodo GET in modo che l'id viene messo nella
	// query string e poi preso da li. come si fa?

	@RequestMapping(value = "/cercaAllievoDaPren", method = RequestMethod.POST)
	public String cercaAllievo(@ModelAttribute Allievo allievo, Model model) {

		Allievo allievoCercato = this.allievoService.findById(allievo.getId());

		if (allievoCercato != null) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			Responsabile user = userService.findByName(auth.getName());

			Centro centroCorrente = user.getCentro();

			model.addAttribute("allievo", allievoCercato);
			model.addAttribute("attivita", this.attivitaService.findByCentro(centroCorrente));

			return "ricercaAttivitaDaPren";
		}

		else {
			model.addAttribute("notExists", "Allievo non trovato");
			return "prenotazione";
		}

	}

	@RequestMapping(value = "/attivita/{id}", method = RequestMethod.GET)
	public String getAttivita(@PathVariable Long id, Model model, @ModelAttribute Allievo allievo) {

		Allievo allievoCorrente = (Allievo) this.allievoService.findByName(allievo.getName());
		Attivita attivitaCorrente = this.attivitaService.findById(id);

		model.addAttribute("attivita", attivitaCorrente);
		model.addAttribute("allievoCorrente", allievoCorrente);
		this.allievoService.update(allievoCorrente, attivitaCorrente);
		this.attivitaService.update(attivitaCorrente, allievoCorrente);

		return "riepilogoPrenotazione";
	}

}
